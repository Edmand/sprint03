USE [SmilesLeader]
GO

/****** Object:  Table [dbo].[CSATDetail]    Script Date: 10/7/2014 2:34:08 PM ******/
DROP TABLE [dbo].[CSATDetail]
GO

/****** Object:  Table [dbo].[CSATDetail]    Script Date: 10/7/2014 2:34:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CSATDetail](
	[StoreNumber] [int] NOT NULL,
	[CouponNumber] [nvarchar](13) NOT NULL,
	[SurveyDttm] [datetime] NOT NULL,
	[TransStartTimeStamp] [datetime] NULL,
	[VisitDate] [date] NULL,
	[R00002] [smallint] NULL,
	[R55020] [smallint] NULL,
	[R54100] [smallint] NULL,
	[R54111] [smallint] NULL,
	[R54121] [smallint] NULL,
	[R54113] [smallint] NULL,
	[R01000] [smallint] NULL,
	[R54160] [smallint] NULL,
	[R11500] [smallint] NULL,
	[R22000] [smallint] NULL,
	[R21000] [smallint] NULL,
	[R27000] [smallint] NULL,
	[R28500] [smallint] NULL,
	[R28000] [smallint] NULL,
	[R00005] [smallint] NULL,
	[R54250] [smallint] NULL,
	[R33000] [smallint] NULL,
	[R00019] [smallint] NULL,
	[R54260] [smallint] NULL,
	[R56010] [smallint] NULL,
	[R00020] [smallint] NULL,
	[R00021] [smallint] NULL,
	[R56020] [smallint] NULL,
	[R54270] [smallint] NULL,
	[R54272] [smallint] NULL,
	[R28600] [smallint] NULL,
	[R56030] [smallint] NULL,
	[R56040] [smallint] NULL,
	[R56050] [smallint] NULL,
	[R56060] [smallint] NULL,
	[R00003] [smallint] NULL,
	[R00004] [smallint] NULL,
	[R00006] [smallint] NULL,
	[R00007] [smallint] NULL,
	[R00008] [smallint] NULL,
	[R00009] [smallint] NULL,
	[R00011] [smallint] NULL,
	[R00012] [smallint] NULL,
	[R00013] [smallint] NULL,
	[R00014] [smallint] NULL,
	[R00015] [smallint] NULL,
	[R54275] [smallint] NULL,
	[R00016] [smallint] NULL,
	[R00017] [smallint] NULL,
	[R00022] [smallint] NULL,
	[R00023] [smallint] NULL,
	[R00024] [smallint] NULL,
	[R00025] [smallint] NULL,
	[R00026] [smallint] NULL,
	[R00027] [smallint] NULL,
	[R00028] [smallint] NULL,
	[R00029] [smallint] NULL,
	[R00030] [smallint] NULL,
	[R00031] [smallint] NULL,
	[R54291] [smallint] NULL,
	[R54292] [smallint] NULL,
	[R54276] [smallint] NULL,
	[R54320] [smallint] NULL,
	[R54330] [smallint] NULL,
	[R00001] [smallint] NULL,
	[S00002] [nvarchar](200) NULL,
	[S87000] [nvarchar](2000) NULL,
	[S81001] [nvarchar](2000) NULL,
	[S81002] [nvarchar](2000) NULL,
	[TransmissionStatus] [char](1) NULL,
	[RecordAdded] [datetime] NULL,
 CONSTRAINT [PK_CSATDetail] PRIMARY KEY CLUSTERED 
(
	[StoreNumber] ASC,
	[CouponNumber] ASC,
	[SurveyDttm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


