USE SmilesLeader
GO

truncate table csatmetricsummary
GO

DECLARE @processDate datetime

SET @processDate = '2014-02-03 00:00:00'

WHILE @processDate <= GETDATE()
BEGIN
	PRINT 'Processing for survey date = ' + CONVERT(VARCHAR, @processDate, 101)
	EXEC usp_CSATMetricSummaryAggregationByDate @processDate
	SET @processDate = DATEADD(day,1,@processDate)
END

PRINT N'Processing completed up to today.'



