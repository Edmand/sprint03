USE [SmilesLeader]
GO
/****** Object:  StoredProcedure [dbo].[usp_CSATMetricSummaryAggregationByDate]    Script Date: 10/3/2014 9:21:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edmand Looi
-- Create date: 10/3/2014
-- Description:	This store procedure will aggregate data from CSATDetail for a specified survey day and insert the result to CSATMetricSummary.
-- =============================================

ALTER PROCEDURE [dbo].[usp_CSATMetricSummaryAggregationByDate]
	-- Add the parameters for the stored procedure here
	@SurveyDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	-- Declare a table variable to temporary store the summary of the CSAT for a specific day.
	DECLARE  @CSAT_Metrics TABLE (
			 StoreNumber int
			,TransStartTimeStamp datetime
			,SurveyCount int
			,Question_Count int
			,MeasuredFeet_Yes_Count int
			,MeasuredFeet_No_Count int
			,AssistInAisle_Yes_Count int
			,AssistInAisle_No_Count int
			,FindShoe_Yes_Count int
			,FindShoe_No_Count int
			,SuggestShoes_Yes_Count int
			,SuggestShoes_No_Count int
			,SuggestAccessories_Yes_Count int
			,SuggestAccessories_No_Count int
			,ExplainPromo_Yes_Count int
			,ExplainPromo_No_Count int
			,OfferOpinion_Yes_Count int
			,OfferOpinion_No_Count int
	)

	-- Update CSATDetail TransStartTimeStamp field if it is null.
	UPDATE CD	
	SET TransStartTimeStamp = DATEADD(MINUTE, -DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101')) + (ROUND(DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101'))/CAST(15 As float),0)*15), DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101'))
	FROM CSATDetail CD
	INNER JOIN StoreTransaction ST ON CD.CouponNumber = ST.ReceiptNumber
	WHERE CD.TransStartTimeStamp IS NULL

	-- Calculate and insert the aggregate of the CSAT for the specific day.
	INSERT INTO @CSAT_Metrics (
			 StoreNumber
			,TransStartTimeStamp
			,SurveyCount
			,Question_Count
			,MeasuredFeet_Yes_Count
			,MeasuredFeet_No_Count
			,AssistInAisle_Yes_Count
			,AssistInAisle_No_Count
			,FindShoe_Yes_Count
			,FindShoe_No_Count
			,SuggestShoes_Yes_Count
			,SuggestShoes_No_Count
			,SuggestAccessories_Yes_Count
			,SuggestAccessories_No_Count
			,ExplainPromo_Yes_Count
			,ExplainPromo_No_Count
			,OfferOpinion_Yes_Count
			,OfferOpinion_No_Count
	) 
	SELECT   CT.StoreNumber
			,MIN(CT.TransStartTimeStamp) AS TransStartTimeStamp
			,COUNT(*) AS SurveyCount
			,SUM(CASE WHEN CT.R54113 = 1 OR CT.R54113 = 3 THEN 1 ELSE 0 END 
				+ CASE WHEN CT.R56010 = 1 OR CT.R56010 = 3 THEN 1 ELSE 0 END
				+ CASE WHEN CT.R56030 = 1 OR CT.R56030 = 3 THEN 1 ELSE 0 END
				+ CASE WHEN CT.R56040 = 1 OR CT.R56040 = 3 THEN 1 ELSE 0 END
				+ CASE WHEN CT.R56050 = 1 OR CT.R56050 = 3 THEN 1 ELSE 0 END
				+ CASE WHEN CT.R56060 = 1 OR CT.R56060 = 3 THEN 1 ELSE 0 END
				+ CASE WHEN CT.R28600 = 1 OR CT.R28600 = 3 THEN 1 ELSE 0 END
				) AS QuestionCount
			,SUM(CASE WHEN CT.R54113 = 1 THEN 1 ELSE 0 END) AS MeasuredFeet_Yes_Count
			,SUM(CASE WHEN CT.R54113 = 3 THEN 1 ELSE 0 END) AS MeasuredFeet_No_Count
			,SUM(CASE WHEN CT.R56010 = 1 THEN 1 ELSE 0 END) AS AssitAisle_Yes_Count
			,SUM(CASE WHEN CT.R56010 = 3  THEN 1 ELSE 0 END) AS AssitAisle_No_Count
			,SUM(CASE WHEN CT.R56030 = 1 THEN 1 ELSE 0 END) AS FindShoe_Yes_Count
			,SUM(CASE WHEN CT.R56030 = 3  THEN 1 ELSE 0 END) AS FindShoe_No_Count
			,SUM(CASE WHEN CT.R56040 = 1 THEN 1 ELSE 0 END) AS SuggestShoes_Yes_Count
			,SUM(CASE WHEN CT.R56040 = 3  THEN 1 ELSE 0 END) AS SuggestShoes_No_Count
			,SUM(CASE WHEN CT.R56050 = 1 THEN 1 ELSE 0 END) AS SuggestAccessories_Yes_Count
			,SUM(CASE WHEN CT.R56050 = 3  THEN 1 ELSE 0 END) AS SuggestAccessories_No_Count
			,SUM(CASE WHEN CT.R56060 = 1 THEN 1 ELSE 0 END) AS ExplainPromo_Yes_Count
			,SUM(CASE WHEN CT.R56060 = 3  THEN 1 ELSE 0 END) AS ExplainPromo_No_Count
			,SUM(CASE WHEN CT.R28600 = 1 THEN 1 ELSE 0 END) AS OfferOpinion_Yes_Count
			,SUM(CASE WHEN CT.R28600 = 3  THEN 1 ELSE 0 END) AS OfferOpinion_No_Count
	FROM CSATDetail CT
	INNER JOIN StoreTransaction ST ON CT.CouponNumber = ST.ReceiptNumber
	INNER JOIN ( 	
			SELECT CD.StoreNumber, DATEADD(MINUTE, -DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', StoreTrans.TransStartTimeStamp), '20080101')) + (ROUND(DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', StoreTrans.TransStartTimeStamp), '20080101'))/CAST(15 As float),0)*15), DATEADD(minute, DATEDIFF(minute, '20080101', StoreTrans.TransStartTimeStamp), '20080101')) AS TransStartTimeStamp
			FROM CSATDetail CD
			INNER JOIN StoreTransaction StoreTrans ON CD.CouponNumber = StoreTrans.ReceiptNumber
			WHERE CONVERT(date, CD.SurveyDttm) = @SurveyDate) AS CU ON CU.StoreNumber = CT.StoreNumber AND CU.TransStartTimeStamp =  CT.TransStartTimeStamp
	GROUP BY CT.StoreNumber, CT.TransStartTimeStamp
	ORDER BY CT.StoreNumber, CT.TransStartTimeStamp

	-- Merge the new aggregate data with CSATMetricSummary table. 
	MERGE CSATMetricSummary AS Target
		USING @CSAT_Metrics AS Source
		ON Target.StoreNumber = Source.StoreNumber AND Target.TransStartTimeStamp = Source.TransStartTimeStamp
	WHEN MATCHED 
		THEN UPDATE SET
			 Target.SurveyCount = Source.SurveyCount
			,Target.Question_Count = Source.Question_Count
			,Target.R54113_MeasuredFeet_Yes_Count = Source.MeasuredFeet_Yes_Count
			,Target.R54113_MeasuredFeet_No_Count = Source.MeasuredFeet_No_Count
			,Target.R56010_AssistInAisle_Yes_Count = Source.AssistInAisle_Yes_Count
			,Target.R56010_AssistInAisle_No_Count = Source.AssistInAisle_No_Count
			,Target.R56030_FindShoe_Yes_Count = Source.FindShoe_Yes_Count
			,Target.R56030_FindShoe_No_Count = Source.FindShoe_No_Count
			,Target.R56040_SuggestShoes_Yes_Count = Source.SuggestShoes_Yes_Count
			,Target.R56040_SuggestShoes_No_Count = Source.SuggestShoes_No_Count
			,Target.R56050_SuggestAccessories_Yes_Count = Source.SuggestAccessories_Yes_Count
			,Target.R56050_SuggestAccessories_No_Count = Source.SuggestAccessories_No_Count
			,Target.R56060_ExplainPromo_Yes_Count = Source.ExplainPromo_Yes_Count
			,Target.R56060_ExplainPromo_No_Count = Source.ExplainPromo_No_Count
			,Target.R28600_OfferOpinion_Yes_Count = Source.OfferOpinion_Yes_Count
			,Target.R28600_OfferOpinion_No_Count = Source.OfferOpinion_No_Count
	WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (
			 StoreNumber
			,TransStartTimeStamp
			,SurveyCount
			,Question_Count
			,R54113_MeasuredFeet_Yes_Count
			,R54113_MeasuredFeet_No_Count
			,R56010_AssistInAisle_Yes_Count
			,R56010_AssistInAisle_No_Count
			,R56030_FindShoe_Yes_Count
			,R56030_FindShoe_No_Count
			,R56040_SuggestShoes_Yes_Count
			,R56040_SuggestShoes_No_Count
			,R56050_SuggestAccessories_Yes_Count
			,R56050_SuggestAccessories_No_Count
			,R56060_ExplainPromo_Yes_Count
			,R56060_ExplainPromo_No_Count
			,R28600_OfferOpinion_Yes_Count
			,R28600_OfferOpinion_No_Count
		)  VALUES (
			 Source.StoreNumber
			,Source.TransStartTimeStamp
			,Source.SurveyCount
			,Source.Question_Count
			,Source.MeasuredFeet_Yes_Count
			,Source.MeasuredFeet_No_Count
			,Source.AssistInAisle_Yes_Count
			,Source.AssistInAisle_No_Count
			,Source.FindShoe_Yes_Count
			,Source.FindShoe_No_Count
			,Source.SuggestShoes_Yes_Count
			,Source.SuggestShoes_No_Count
			,Source.SuggestAccessories_Yes_Count
			,Source.SuggestAccessories_No_Count
			,Source.ExplainPromo_Yes_Count
			,Source.ExplainPromo_No_Count
			,Source.OfferOpinion_Yes_Count
			,Source.OfferOpinion_No_Count
	);

END
