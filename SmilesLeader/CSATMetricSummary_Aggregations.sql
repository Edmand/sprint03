USE SmilesLeader
GO

-- Declare temporary table variable
DECLARE  @CSAT_TEMP table (
		 StoreNumber int 
		,TransStartTimeStamp datetime
		,CouponNumber varchar(13)
		--,Q1_Id nvarchar(10)
		--,Q2_Id nvarchar(10)
		--,Q3_Id nvarchar(10)
		--,Q4_Id nvarchar(10)
		--,Q5_Id nvarchar(10)
		--,Q6_Id nvarchar(10)
		--,Q7_Id nvarchar(10)
		)

--INSERT INTO @CSAT_TEMP
--SELECT  CD.StoreNumber AS StoreNumber
--	   ,DATEADD(MINUTE, -DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101')) + (ROUND(DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101'))/CAST(15 As float),0)*15), DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101')) AS TransStartTimeStamp
--	   ,CouponNumber
--	   --,'R54113' AS Q1_Id
--	   --,'R56010' AS Q2_Id
--	   --,'R56030' AS Q3_Id
--	   --,'R56040' AS Q4_Id
--	   --,'R56050' AS Q5_Id
--	   --,'R56060' AS Q6_Id
--	   --,'R28600' AS Q7_Id
--FROM CSATDetail CD
--INNER JOIN StoreTransaction ST ON CD.CouponNumber = ST.ReceiptNumber
--WHERE CD.StoreNumber = 4804

--SELECT * FROM @CSAT_TEMP

SELECT  CT.StoreNumber
	   ,CT.TransStartTimeStamp
	   ,'R54113' AS Q1_Id
	   ,'R56010' AS Q2_Id
	   ,'R56030' AS Q3_Id
	   ,'R56040' AS Q4_Id
	   ,'R56050' AS Q5_Id
	   ,'R56060' AS Q6_Id
	   ,'R28600' AS Q7_Id
	   ,SUM(CASE WHEN CT.R54113 = 1 THEN 1 ELSE 0 END) AS MeasuredFeet_Yes_Count
	   ,SUM(CASE WHEN CT.R54113 = 1 OR CT.R54113 = 3 THEN 1 ELSE 0 END) AS MeasuredFeet_Total_Count
	   ,SUM(CASE WHEN CT.R56010 = 1 THEN 1 ELSE 0 END) AS AssitAisle_Yes_Count
	   ,SUM(CASE WHEN CT.R56010 = 1 OR CT.R56010 = 3  THEN 1 ELSE 0 END) AS AssitAisle_Total_Count
	   ,SUM(CASE WHEN CT.R56030 = 1 THEN 1 ELSE 0 END) AS FindShoe_Yes_Count
	   ,SUM(CASE WHEN CT.R56030 = 1 OR CT.R56030 = 3  THEN 1 ELSE 0 END) AS FindShoe_Total_Count
	   ,SUM(CASE WHEN CT.R56040 = 1 THEN 1 ELSE 0 END) AS SuggestShoes_Yes_Count
	   ,SUM(CASE WHEN CT.R56040 = 1 OR CT.R56040 = 3  THEN 1 ELSE 0 END) AS SuggestShoes_Total_Count
	   ,SUM(CASE WHEN CT.R56050 = 1 THEN 1 ELSE 0 END) AS SuggestAccessories_Yes_Count
	   ,SUM(CASE WHEN CT.R56050 = 1 OR CT.R56050 = 3  THEN 1 ELSE 0 END) AS SuggestAccessories_Total_Count
	   ,SUM(CASE WHEN CT.R56060 = 1 THEN 1 ELSE 0 END) AS ExplainPromo_Yes_Count
	   ,SUM(CASE WHEN CT.R56060 = 1 OR CT.R56060 = 3  THEN 1 ELSE 0 END) AS ExplainPromo_Total_Count
	   ,SUM(CASE WHEN CT.R28600 = 1 THEN 1 ELSE 0 END) AS OfferOpinion_Yes_Count
	   ,SUM(CASE WHEN CT.R28600 = 1 OR CT.R28600 = 3  THEN 1 ELSE 0 END) AS OfferOpinion_Total_Count
	   ,COUNT(*) AS CSAT_Count
FROM (
		SELECT CD.*, DATEADD(MINUTE, -DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101')) + (ROUND(DATEPART(MINUTE, DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101'))/CAST(15 As float),0)*15), DATEADD(minute, DATEDIFF(minute, '20080101', ST.TransStartTimeStamp), '20080101')) AS TransStartTimeStamp
		FROM CSATDetail CD
		INNER JOIN StoreTransaction ST ON CD.CouponNumber = ST.ReceiptNumber
		WHERE CD.StoreNumber = 4804
) AS CT
WHERE CT.StoreNumber = 4804
GROUP BY CT.StoreNumber, CT.TransStartTimeStamp
ORDER BY CT.StoreNumber, CT.TransStartTimeStamp







