USE SmilesLeader
GO

-- Declare temporary table variable
DECLARE  @CSAT_TEMP table (
			StoreNumber int 
		,MeasuredFeet_Yes_Count int
		,MeasuredFeet_Total_Count int
		,AssitAisle_Yes_Count int
		,AssitAisle_Total_Count int
		,FindShoe_Yes_Count int
		,FindShoe_Total_Count int
		,SuggestShoes_Yes_Count int
		,SuggestShoes_Total_Count int
		,SuggestAccessories_Yes_Count int
		,SuggestAccessories_Total_Count int
		,ExplainPromo_Yes_Count int
		,ExplainPromo_Total_Count int
		,OfferOpinion_Yes_Count int
		,OfferOpinion_Total_Count int
		,CSAT_Count int
		)

INSERT INTO @CSAT_TEMP 
SELECT  StoreNumber
	   ,SUM(CASE WHEN R54113 = 1 THEN 1 ELSE 0 END) AS MeasuredFeet_Yes_Count
	   ,SUM(CASE WHEN R54113 = 1 OR R54113 = 3 THEN 1 ELSE 0 END) AS MeasuredFeet_Total_Count
	   ,SUM(CASE WHEN R56010 = 1 THEN 1 ELSE 0 END) AS AssitAisle_Yes_Count
	   ,SUM(CASE WHEN R56010 = 1 OR R56010 = 3  THEN 1 ELSE 0 END) AS AssitAisle_Total_Count
	   ,SUM(CASE WHEN R56030 = 1 THEN 1 ELSE 0 END) AS FindShoe_Yes_Count
	   ,SUM(CASE WHEN R56030 = 1 OR R56030 = 3  THEN 1 ELSE 0 END) AS FindShoe_Total_Count
	   ,SUM(CASE WHEN R56040 = 1 THEN 1 ELSE 0 END) AS SuggestShoes_Yes_Count
	   ,SUM(CASE WHEN R56040 = 1 OR R56040 = 3  THEN 1 ELSE 0 END) AS SuggestShoes_Total_Count
	   ,SUM(CASE WHEN R56050 = 1 THEN 1 ELSE 0 END) AS SuggestAccessories_Yes_Count
	   ,SUM(CASE WHEN R56050 = 1 OR R56050 = 3  THEN 1 ELSE 0 END) AS SuggestAccessories_Total_Count
	   ,SUM(CASE WHEN R56060 = 1 THEN 1 ELSE 0 END) AS ExplainPromo_Yes_Count
	   ,SUM(CASE WHEN R56060 = 1 OR R56060 = 3  THEN 1 ELSE 0 END) AS ExplainPromo_Total_Count
	   ,SUM(CASE WHEN R28600 = 1 THEN 1 ELSE 0 END) AS OfferOpinion_Yes_Count
	   ,SUM(CASE WHEN R28600 = 1 OR R28600 = 3  THEN 1 ELSE 0 END) AS OfferOpinion_Total_Count
	   ,COUNT(*) AS CSAT_Count
FROM CSATDetail
WHERE VisitDate BETWEEN '2014-05-04' AND '2014-08-02'
GROUP BY StoreNumber
ORDER BY StoreNumber
	--AND StoreNumber = 11


SELECT * FROM @CSAT_TEMP WHERE StoreNumber IN(1,11,12) ORDER BY StoreNumber

SELECT  StoreNumber
	   ,CSAT_Count AS SurveyCount
	   ,OfferOpinion_Yes_Count + MeasuredFeet_Yes_Count + AssitAisle_Yes_Count + FindShoe_Yes_Count + SuggestShoes_Yes_Count + SuggestAccessories_Yes_Count + ExplainPromo_Yes_Count AS TotalYes
	   ,OfferOpinion_Total_Count + MeasuredFeet_Total_Count + AssitAisle_Total_Count + FindShoe_Total_Count + SuggestShoes_Total_Count + SuggestAccessories_Total_Count + ExplainPromo_Total_Count AS TotalAnswer
	   ,CAST(ROUND(CAST((OfferOpinion_Yes_Count + MeasuredFeet_Yes_Count + AssitAisle_Yes_Count + FindShoe_Yes_Count + SuggestShoes_Yes_Count + SuggestAccessories_Yes_Count + ExplainPromo_Yes_Count) AS DECIMAL(8,1)) /
		   CAST((OfferOpinion_Total_Count + MeasuredFeet_Total_Count + AssitAisle_Total_Count + FindShoe_Total_Count + SuggestShoes_Total_Count + SuggestAccessories_Total_Count + ExplainPromo_Total_Count) AS DECIMAL(8,1)) * 100,1) AS DECIMAL(4,1)) AS SmilesScores
	   ,CAST(ROUND(OfferOpinion_Yes_Count / CAST(NULLIF(OfferOpinion_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1)) AS OfferOpinion
	   ,CAST(ROUND(CAST(MeasuredFeet_Yes_Count AS DECIMAL(4,1)) / CAST(NULLIF(MeasuredFeet_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1))  AS MeasuredFeet
	   ,CAST(ROUND(CAST(AssitAisle_Yes_Count AS DECIMAL(4,1)) / CAST(NULLIF(AssitAisle_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1))  AS AssitanceInAisle
	   ,CAST(ROUND(CAST(FindShoe_Yes_Count AS DECIMAL(4,1)) / CAST(NULLIF(FindShoe_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1))  AS FindShoe
	   ,CAST(ROUND(CAST(SuggestShoes_Yes_Count AS DECIMAL(4,1)) / CAST(NULLIF(SuggestShoes_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1))  AS SuggestShoes
	   ,CAST(ROUND(CAST(SuggestAccessories_Yes_Count AS DECIMAL(4,1)) / CAST(NULLIF(SuggestAccessories_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1))  AS SuggestAccessories
	   ,CAST(ROUND(CAST(ExplainPromo_Yes_Count AS DECIMAL(4,1)) / CAST(NULLIF(ExplainPromo_Total_Count,0) AS DECIMAL(4,1)) * 100,1) AS DECIMAL(4,1))  AS ExplainPromo
FROM @CSAT_TEMP
ORDER BY StoreNumber


--SELECT * FROM @CSAT_TEMP

