Use SmilesLeader
GO

SELECT  [StoreNumber] 
       ,[CouponNumber] 
       ,[SurveyDttm]  
	   ,COUNT(*) AS DuplicateCount
FROM [CSATDetail] 
GROUP BY [StoreNumber],[CouponNumber],[SurveyDttm] 
HAVING COUNT(*) >1

SELECT * FROM CSATDetail WHERE StoreNumber IS NULL
SELECT * FROM CSATDetail WHERE CouponNumber IS NULL
SELECT * FROM CSATDetail WHERE SurveyDttm IS NULL
SELECT * FROM CSATDetail WHERE VisitDate IS NULL

SELECT TOP 100 * FROM CSATDetail

SELECT COUNT(*) AS RecordCount FROM CSATDetail
