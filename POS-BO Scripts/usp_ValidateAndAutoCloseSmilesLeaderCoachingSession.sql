USE [GlobalSTORE]
GO
/****** Object:  StoredProcedure [dbo].[usp_ValidateAndAutoCloseSmilesLeaderCoachingSession]    Script Date: 10/1/2014 2:29:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edmand Looi
-- Create date: 10/1/2014
-- Description:	This store procedure is called by a timer/job. 
--				This store procedure will auto close the coaching session when the coaching duration has reach it maximum duration. 
-- =============================================
ALTER PROCEDURE [dbo].[usp_ValidateAndAutoCloseSmilesLeaderCoachingSession]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @currentCoachEventID int
	       ,@maxCoachingDurationInMinutes int
		   ,@currentDateTime datetime
		   ,@nullDate datetime
		   ,@expectedCoachingEndDateTime datetime
		   ,@zero int
		   ,@SmilesLeaderName nvarchar(100)
		   ,@currentMinute int
		   ,@timeSliceEnd datetime
	
	SET @zero = 0
	SET @currentCoachEventID = @zero
	SET @maxCoachingDurationInMinutes = @zero
	SET @currentDateTime = DATEADD(minute, DATEDIFF(minute, '20080101', GetDate()), '20080101')
	SET @nullDate = '2999-12-31 00:00:00.000'
	SET @expectedCoachingEndDateTime = '1900-01-01 00:00:00'
	SET @SmilesLeaderName = ''
	SET @currentMinute = @zero
	SET @timeSliceEnd = @nullDate

	-- Get the latest smiles leader coaching record.
	SELECT @currentCoachEventID = CoachEventID
	FROM RGS_SmilesLeaderCoachEvent
	WHERE UntilDttm = @nullDate

	-- Get maximun duration in minutes.
	SELECT TOP 1 
		@maxCoachingDurationInMinutes = ParmValue
	FROM RGS_Parm_Store_Value
	WHERE ParmID = 736 AND @currentDateTime >= EffDate
	ORDER BY EffDate DESC

	-- Check if there is a coaching record.
	IF (@currentCoachEventID > @zero AND @maxCoachingDurationInMinutes > 0)	-- Currently there is a coaching session AND coaching timeout is greater than zero.
		BEGIN
			-- Calculate the expected coaching end datetime
			SELECT @expectedCoachingEndDateTime = DATEADD(MINUTE, @maxCoachingDurationInMinutes, StartDttm)
			FROM RGS_SmilesLeaderCoachEvent
			WHERE CoachEventID = @currentCoachEventID

			-- Calculate the ending of the 15 minutes time slice.
			SET @currentMinute = DATEPART(MINUTE, @currentDateTime)
			SELECT @timeSliceEnd = DATEADD(MINUTE, -@currentMinute + (ROUND(@currentMinute/CAST(15 As float),0)*15), @currentDateTime )

			-- Check if maximun duratin has reached since the coaching session started.
			IF (@currentDateTime >= @expectedCoachingEndDateTime) -- Coaching maximum duration has occur 
				BEGIN 
					-- Close the current coaching session.
					UPDATE RGS_SmilesLeaderCoachEvent SET
						 UntilDttm = @timeSliceEnd
						,UpdatedBy = 'usp_ValidateAndAutoCloseSmilesLeaderCoachingSession'
						,DtStamp = GETDATE()
					WHERE CoachEventID = @currentCoachEventID

					-- Insert the closed smile leader coaching session into RGS_DataCollect table
					EXEC usp_InsertSmilesLeaderCoachIntoDataCollect @currentCoachEventID

					-- Get current smiles leader record.
					SELECT  @SmilesLeaderName = Emp.FirstName + ' ' + Emp.LastName	
					FROM RGS_SmilesLeaderEvent SL 
					INNER JOIN RGS_Employee Emp ON SL.EmplID = Emp.EmplID
					WHERE UntilDttm = @nullDate

					-- Update RGS_System_Values table with new Smiles Leader.
					UPDATE RGS_System_Values SET
						 SystValue = @SmilesLeaderName
						,DtStamp = GETDATE()
					WHERE SystId = 'SmilesLeaderName'
				END
		END
END
