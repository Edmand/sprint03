USE [GlobalSTORE]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSmilesLeaderCoachIntoDataCollect]    Script Date: 10/2/2014 3:37:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edmand Looi
-- Create date: 10/2/2014
-- Description:	Insert Smiles Leader coach record into data collect table.
-- =============================================

ALTER PROCEDURE [dbo].[usp_InsertSmilesLeaderCoachIntoDataCollect] 
	-- Add the parameters for the stored procedure here
	@smilesLeaderCoachEventID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	-- Insert coaching record into RGS_DataCollect table
	INSERT INTO RGS_DataCollect (
		 Dc_Code
		,Dc_Version
		,Store_No
		,Dc_XML_Obj
		,Transmit_DtTm
		,DateStamp)
	SELECT
		'SMILES'
		,'1'
		,StoreNum
		,'<?xml version="1.0"?><DS_SMILESLEADERCOACH><SMILESLEADERCOACH><StoreNumber>'+CONVERT(varchar(12), StoreNum)+'</StoreNumber><StartDttm>'+convert(varchar(25), StartDttm, 121)+'</StartDttm><UntilDttm>'+convert(varchar(25), UntilDttm, 121)+'</UntilDttm><CoachedByCd>'+CoachedByCd+'</CoachedByCd><CoachedByEmplID>'+CoachedByEmplID+'</CoachedByEmplID><SmilesLeaderCoachEventID>'+CONVERT(varchar(12), CoachEventID)+'</SmilesLeaderCoachEventID></SMILESLEADERCOACH></DS_SMILESLEADERCOACH>'
		,{ts'1900-01-01 00:00:00'}
		,GETDATE()
	FROM RGS_SmilesLeaderCoachEvent
	WHERE CoachEventID = @smilesLeaderCoachEventID	


	RETURN

END
